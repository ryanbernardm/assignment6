function createTable()
{
	var xhttp = new XMLHttpRequest();
	var yearCount;
	var tableContents = "<tr><th>Year</th><th>Number of meetings</th></tr>";
	xhttp.open("GET", "/app/path/meetings-resource2013", false);
	xhttp.send();
	yearCount = xhttp.responseText;
	tableContents += "<tr><td>2013</td><td>" + yearCount + "</td></tr>";
	
	xhttp = new XMLHttpRequest();
	xhttp.open("GET", "/app/path/meetings-resource2014", false);
	xhttp.send();
	yearCount = xhttp.responseText;
	tableContents += "<tr><td>2014</td><td>" + yearCount + "</td></tr>";
	
	xhttp = new XMLHttpRequest();
	xhttp.open("GET", "/app/path/meetings-resource2015", false);
	xhttp.send();
	yearCount = xhttp.responseText;
	tableContents += "<tr><td>2015</td><td>" + yearCount + "</td></tr>";
	
	xhttp = new XMLHttpRequest();
	xhttp.open("GET", "/app/path/meetings-resource2016", false);
	xhttp.send();
	yearCount = xhttp.responseText;
	tableContents += "<tr><td>2016</td><td>" + yearCount + "</td></tr>";
	
	xhttp = new XMLHttpRequest();
	xhttp.open("GET", "/app/path/meetings-resource2017", false);
	xhttp.send();
	yearCount = xhttp.responseText;
	tableContents += "<tr><td>2017</td><td>" + yearCount + "</td></tr>";
	document.getElementById("meetingTable").innerHTML = tableContents;
}