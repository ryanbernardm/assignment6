package assign.services;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;



public class EavesdropService {
	
	
	public int getMeetings(String url, String year) throws IOException{
		int count = 0;
			Document document = Jsoup.connect(url).get();
			
			Elements links = document.getElementsByAttributeValueMatching("href", year + "/");
			if(links == null || links.isEmpty()){
				return count;
			}
			document = Jsoup.connect(links.attr("abs:href")).get();
	        links = document.select("body a");
	        //skip first 5 elements due to webpage layout
	        for(int i = 5; i < links.size(); i+=4){ //hardcode OK
	        	count++;
			}
		
		return count;
	}
	
}