package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import org.jsoup.select.Elements;
import assign.services.EavesdropService;

@Path("/path")
public class MeetingsResource {
	
	String url;
	EavesdropService eavesdropService;
	
	public MeetingsResource() {
		this.url = "http://eavesdrop.openstack.org/meetings/solum_team_meeting/";
		this.eavesdropService = new EavesdropService();
	}

	@GET
	@Path("/meetings-resource2013")
	@Produces("text/xml")
	public String getMeetings2013() throws Exception {
		Integer count;
		count = eavesdropService.getMeetings(url, "2013");
		return count.toString();
	}
	
	@GET
	@Path("/meetings-resource2014")
	@Produces("text/xml")
	public String getMeetings2014() throws Exception {
		Integer count;
		count = eavesdropService.getMeetings(url, "2014");
		return count.toString();
	}
	
	@GET
	@Path("/meetings-resource2015")
	@Produces("text/xml")
	public String getMeetings2015() throws Exception {
		Integer count;
		count = eavesdropService.getMeetings(url, "2015");
		return count.toString();
	}
	
	@GET
	@Path("/meetings-resource2016")
	@Produces("text/xml")
	public String getMeetings2016() throws Exception {
		Integer count;
		count = eavesdropService.getMeetings(url, "2016");
		return count.toString();
	}
	
	@GET
	@Path("/meetings-resource2017")
	@Produces("text/xml")
	public String getMeetings2017() throws Exception {
		Integer count;
		count = eavesdropService.getMeetings(url, "2017");
		return count.toString();
	}
	
	@GET
	@Path("/helloworld-resource")
	@Produces("text/html")
	public String helloWorld() {
		System.out.println("Inside helloworld");
		return "Hello world ";
	}
	
	@GET
	@Path("/helloworld-austin")
	@Produces("text/html")
	public String helloAustin() {
		System.out.println("Inside helloAustin");
		return "Hello Austin ";
	}

}